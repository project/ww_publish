<?php

namespace Drupal\Tests\ww_publish\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\ww_publish\Entity\SnsMessageEntity;
use Drupal\ww_publish\Entity\SnsMessageEntityInterface;

/**
 * Tests the SNS Message form.
 *
 * @group ww_publish
 */
class WwPublishMessageFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['ww_publish'];

  /**
   * Default theme.
   *
   * See: https://www.drupal.org/node/3083055
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * An administrative user account that can administer text formats.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Perform any initial set up tasks that run before every test method.
   *
   * Info to administrator permissions:
   * http://drupal.stackexchange.com/q/233416/72107
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer site configuration']);
    $node_type = NodeType::create([
      'type' => 'article',
      'name' => 'article',
    ]);
    $node_type->save();

    $this->config('ww_publish.settings')->set('content_type_field', 'article')->save();
    FieldStorageConfig::create([
      'field_name' => 'field_test_ww_id',
      'entity_type' => 'node',
      'type' => 'string',
    ])->save();
    FieldConfig::create([
      'label' => 'WW ID',
      'field_name' => 'field_test_ww_id',
      'entity_type' => 'node',
      'bundle' => 'article',
    ])->save();
    FieldStorageConfig::create([
      'field_name' => 'field_test_story_id',
      'entity_type' => 'node',
      'type' => 'string',
    ])->save();
    FieldConfig::create([
      'label' => 'WW ID',
      'field_name' => 'field_test_story_id',
      'entity_type' => 'node',
      'bundle' => 'article',
    ])->save();
  }

  /**
   * Tests the SNS Message form.
   */
  public function testMessageForm() {
    $this->drupalLogin($this->adminUser);
    $node = Node::create([
      'type' => 'article',
      'title' => 'Article with old ID',
      'field_test_ww_id' => 111,
    ]);
    $node->save();

    // Create a SNS Message with no matching Article ID and Story ID.
    $message = SnsMessageEntity::create([
      'id' => '333',
      'name' => 'not_configured_test',
      'url' => $this->getFixturePath('test_article', 'article.zip'),
      'metadata_url' => $this->getFixturePath('test_article', 'metadata.json'),
      'article_json_url' => $this->getFixturePath('test_article', 'article.json'),
      'tenant_id' => '1',
      'brand' => 'test',
      'status' => SnsMessageEntityInterface::IMPORTED,
    ]);
    $message->save();
    $message = SnsMessageEntity::load($message->id());
    $this->drupalGet('admin/config/services/ww-publish/sns-messages/' . $message->id());
    $this->assertSession()->pageTextNotContains("Article's links:");
    $this->assertSession()->linkNotExists('Edit the article in Drupal');
    $this->assertSession()->linkNotExists('Display the article');

    // Delete an SNS Message.
    $this->drupalGet('admin/config/services/ww-publish/sns-messages/');
    $this->clickLink('Delete');
    $this->submitForm([], 'Delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The sns message not_configured_test has been deleted.');
    $this->assertSession()->pageTextContains('There are no sns message entities yet.');

    // Create a SNS Message with matching Article ID.
    $node_type = NodeType::load('article');
    $node_type->setThirdPartySetting('ww_publish', 'article_id_field', 'field_test_ww_id');
    $node_type->save();
    $message = SnsMessageEntity::create([
      'id' => '111',
      'name' => 'test',
      'url' => $this->getFixturePath('test_article', 'article.zip'),
      'metadata_url' => $this->getFixturePath('test_article', 'metadata.json'),
      'article_json_url' => $this->getFixturePath('test_article', 'article.json'),
      'tenant_id' => '1',
      'brand' => 'test',
      'status' => SnsMessageEntityInterface::IMPORTED,
    ]);
    $message->save();
    $message = SnsMessageEntity::load($message->id());
    $this->drupalGet('admin/config/services/ww-publish/sns-messages/' . $message->id());
    $this->assertSession()->pageTextContains("Article's links:");
    $this->assertSession()->linkExists('Edit the article in Drupal');
    $this->assertSession()->linkExists('Display the article');

    // Create a SNS Message with matching Story ID.
    $node_type = NodeType::load('article');
    $node_type->setThirdPartySetting('ww_publish', 'article_storyid_field', 'field_test_story_id');
    $node_type->save();
    $node = Node::create([
      'type' => 'article',
      'title' => 'Article with old ID',
      'field_test_story_id' => 'f4c16641-f483-5b66-b5bc-cf485e658e4b',
    ]);
    $node->save();
    $message = SnsMessageEntity::create([
      'id' => '222',
      'name' => 'test',
      'url' => $this->getFixturePath('test_article', 'article.zip'),
      'metadata_url' => $this->getFixturePath('test_article', 'metadata.json'),
      'article_json_url' => $this->getFixturePath('test_article', 'article.json'),
      'tenant_id' => '1',
      'brand' => 'test',
      'status' => SnsMessageEntityInterface::IMPORTED,
    ]);
    $message->save();
    $message = SnsMessageEntity::load($message->id());
    $this->drupalGet('admin/config/services/ww-publish/sns-messages/' . $message->id());
    $this->assertSession()->pageTextContains("Article's links:");
    $this->assertSession()->linkExists('Edit the article in Drupal');
    $this->assertSession()->linkExists('Display the article');
  }

  /**
   * Returns the relative path to a fixture resource.
   *
   * @param string $article
   *   The name of the test article fixture.
   * @param string $file_name
   *   The filename, article.json, metadata.json or article.zip.
   *
   * @return string
   *   The path to the fixture file.
   */
  protected function getFixturePath($article, $file_name) {
    return \Drupal::service("extension.list.module")->getPath('ww_publish') . '/tests/fixtures/' . $article . '/' . $file_name;
  }

}
