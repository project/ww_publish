<?php

namespace Drupal\Tests\ww_publish\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the settings form.
 *
 * @group ww_publish
 */
class WwPublishSettingsFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['ww_publish'];

  /**
   * Default theme.
   *
   * See: https://www.drupal.org/node/3083055
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * An administrative user account that can administer text formats.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * An basic user account that can only access basic HTML text format.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $guestUser;

  /**
   * Perform any initial set up tasks that run before every test method.
   *
   * Info to administrator permissions:
   * http://drupal.stackexchange.com/q/233416/72107
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp(): void {
    parent::setUp();
    // Create users.
    $this->adminUser = $this->drupalCreateUser(['administer site configuration']);
    $this->guestUser = $this->drupalCreateUser();
  }

  /**
   * Test the settings form.
   *
   * Test, that the '/admin/config/services/ww-publish' path returns
   * the right content and can be saved.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testSettingsFormAsAdmin() {

    $this->drupalLogin($this->adminUser);

    // Test the empty form.
    $this->drupalGet('/admin/config/services/ww-publish');
    $this->assertSession()->statusCodeEquals('200');

    $page = $this->getSession()->getPage();
    $page->hasContent('WoodWing Studio');

    // Test saving the form.
    $page->hasField('content_type_field');
    $page->fillField('content_type_field', 'just_a_string');

    $page->hasField('ww_path');
    $page->fillField('ww_path', 'ww');

    $page->pressButton('Save configuration');

    // Test the updated form.
    $page = $this->getSession()->getPage();
    $page->hasContent('The configuration options have been saved.');

    $page->hasField('content_type_field');
    $field = $page->findField('content_type_field');
    $this->assertEquals('just_a_string', $field->getValue());

    $page->hasField('ww_path');
    $field = $page->findField('ww_path');
    $this->assertEquals('ww', $field->getValue());
  }

  /**
   * Test settings as guest.
   *
   * Tests that the '/admin/config/services/ww-publish' path is not
   * accessible for guests.
   */
  public function testSettingsFormAsGuest() {
    $this->drupalLogin($this->guestUser);

    $this->drupalGet('/admin/config/services/ww-publish');
    $this->assertSession()->statusCodeEquals(403);
  }

}
