<?php

namespace Drupal\Tests\ww_publish\Kernel;

use Drupal\ww_publish\Entity\SnsMessageEntity;
use Drupal\ww_publish\Entity\SnsMessageEntityInterface;

/**
 * Tests Woodwing Publish cron functionality.
 *
 * @group ww_publish
 */
class WwPublishCronTest extends WwPublishTestBase {

  /**
   * Tests SNS Message entity cleanup.
   */
  public function testCleanup() {
    $SnsMessage = SnsMessageEntity::create([
      'id' => '1',
      'name' => 'test',
      'url' => 'https://www.google.com',
      'metadata_url' => 'https://www.google.com',
      'article_json_url' => 'https://www.google.com',
      'tenant_id' => '1',
      'brand' => 'test',
      'status' => SnsMessageEntityInterface::IMPORTED,
      'created' => \Drupal::time()->getRequestTime() - (\Drupal::config('ww_publish.settings')->get('sns_delete') * 25 * 60 * 60),
    ]);
    $SnsMessage->save();
    $SnsMessage2 = SnsMessageEntity::create([
      'id' => '2',
      'name' => 'test',
      'url' => 'https://www.google.com',
      'metadata_url' => 'https://www.google.com',
      'article_json_url' => 'https://www.google.com',
      'tenant_id' => '1',
      'brand' => 'test',
      'status' => SnsMessageEntityInterface::IMPORTED,
      'created' => \Drupal::time()->getRequestTime() - (\Drupal::config('ww_publish.settings')->get('sns_delete') * 23 * 60 * 60),
    ]);
    $SnsMessage2->save();
    $SnsMessageId = $SnsMessage->id();
    $SnsMessageId2 = $SnsMessage2->id();
    \ww_publish_cron();
    $this->assertNull(SnsMessageEntity::load($SnsMessageId));
    $this->assertNotNull(SnsMessageEntity::load($SnsMessageId2));
  }

}
