<?php

namespace Drupal\Tests\ww_publish\Kernel;

use Aws\Sns\Message;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\node\Entity\Node;
use Drupal\ww_publish\Entity\SnsMessageEntity;
use Drupal\ww_publish\Entity\SnsMessageEntityInterface;
use Symfony\Component\ErrorHandler\BufferingLogger;

/**
 * Tests Woodwing Publish SNS event functionality.
 *
 * @group ww_publish
 */
class WwPublishEventTest extends WwPublishTestBase {

  /**
   * Tests successful messages.
   */
  public function testAcceptedMessages() {

    $logger = new BufferingLogger();
    $this->container->get('logger.factory')->addLogger($logger);
    $data = [
      'MessageId' => '1',
      'Timestamp' => '',
      'TopicArn' => 'test:ecs-export-topic:test',
      'Type' => 'Notification',
      'Signature' => '',
      'SigningCertURL' => '',
      'SignatureVersion' => '',
      'Message' => \json_encode([
        'id' => '1',
        'name' => 'name',
        'url' => $this->getFixturePath('test_article', 'article.zip'),
        'metadataUrl' => $this->getFixturePath('test_article', 'metadata.json'),
        'articleJsonUrl' => $this->getFixturePath('test_article', 'article.json'),
        'tenantId' => '1',
        'brand' => 'test',
      ])
    ];

    $message = new Message($data);
    \Drupal::service('amazon_sns.message_dispatcher')->dispatch($message);
    $logs = $logger->cleanLogs();

    $this->assertEquals(RfcLogLevel::WARNING, $logs[0][0]);
    $this->assertEquals('Accepted unconfirmed sns message from TopicArn @topicarn.', $logs[0][1]);
    $this->assertEquals('test:ecs-export-topic:test', $logs[0][2]['@topicarn']);

    /** @var \Drupal\ww_publish\Entity\SnsMessageEntityInterface $message */
    $message_entity = SnsMessageEntity::load(1);
    $this->assertEquals('name', $message_entity->label());
    $this->assertEquals('test', $message_entity->getBrand());
    $this->assertEquals('1', $message_entity->getTenantId());
    $this->assertFalse($message_entity->isExecuted());
    $this->assertStringContainsString('test_article/article.zip', $message_entity->getZipUrl());
    $this->assertStringContainsString('test_article/metadata.json', $message_entity->getMetadataUrl());
    $this->assertStringContainsString('test_article/article.json', $message_entity->getArticleJsonUrl());

    // Simulate that the message has been processed.
    $message_entity->set('status', SnsMessageEntityInterface::IMPORTED);
    $message_entity->save();
    $this->assertTrue($message_entity->isExecuted());

    // Set the explicit topic ARN and simulate an event for the same ID.
    $this->config('ww_publish.settings')
      ->set('topic_arn', ['test:ecs-export-topic:test'])
      ->save();

    $data = [
      'MessageId' => '1',
      'Timestamp' => '',
      'TopicArn' => 'test:ecs-export-topic:test',
      'Type' => 'Notification',
      'Signature' => '',
      'SigningCertURL' => '',
      'SignatureVersion' => '',
      'Message' => \json_encode([
        'id' => '1',
        'name' => 'name update',
        'url' => $this->getFixturePath('test_article2', 'article.zip'),
        'metadataUrl' => $this->getFixturePath('test_article2', 'metadata.json'),
        'articleJsonUrl' => $this->getFixturePath('test_article2', 'article.json'),
        'tenantId' => '2',
        'brand' => 'test update',
      ])
    ];
    $message = new Message($data);
    \Drupal::service('amazon_sns.message_dispatcher')->dispatch($message);
    \Drupal::service('ww_publish.notification_logger_subscriber')->destruct();
    $this->assertEmpty(Node::loadMultiple());

    $logs = $logger->cleanLogs();
    $this->assertCount(0, $logs);

    $message_entity = SnsMessageEntity::load(1);
    $this->assertEquals('name update', $message_entity->label());
    $this->assertEquals('test update', $message_entity->getBrand());
    $this->assertEquals('2', $message_entity->getTenantId());
    $this->assertFalse($message_entity->isExecuted());
    $this->assertStringContainsString('test_article2/article.zip', $message_entity->getZipUrl());
    $this->assertStringContainsString('test_article2/metadata.json', $message_entity->getMetadataUrl());
    $this->assertStringContainsString('test_article2/article.json', $message_entity->getArticleJsonUrl());
  }

  /**
   * Tests invalid messages.
   */
  public function testInvalidMessages() {

    $logger = new BufferingLogger();
    $this->container->get('logger.factory')->addLogger($logger);
    $data = [
      'MessageId' => '1',
      'Timestamp' => '',
      'TopicArn' => 'unknown-topic-arn',
      'Type' => 'Notification',
      'Signature' => '',
      'SigningCertURL' => '',
      'SignatureVersion' => '',
      'Message' => \json_encode([
        'id' => '1',
        'name' => 'name',
        'url' => $this->getFixturePath('test_article', 'article.zip'),
        'metadataUrl' => $this->getFixturePath('test_article', 'metadata.json'),
        'articleJsonUrl' => $this->getFixturePath('test_article', 'article.json'),
        'tenantId' => '1',
        'brand' => 'test',
      ])
    ];

    $message = new Message($data);
    \Drupal::service('amazon_sns.message_dispatcher')->dispatch($message);
    $logs = $logger->cleanLogs();

    $this->assertEquals(RfcLogLevel::ERROR, $logs[0][0]);
    $this->assertEquals('Not processing Amazon SNS message with TopicArn @topicarn.', $logs[0][1]);
    $this->assertEquals('unknown-topic-arn', $logs[0][2]['@topicarn']);

    $this->assertCount(0, SnsMessageEntity::loadMultiple());

    // Set the explicit topic ARN and simulate an event for a different
    $this->config('ww_publish.settings')
      ->set('topic_arn', ['test:ecs-export-topic:test'])
      ->save();

    $data = [
      'MessageId' => '1',
      'Timestamp' => '',
      'TopicArn' => 'notatest:ecs-export-topic:test',
      'Type' => 'Notification',
      'Signature' => '',
      'SigningCertURL' => '',
      'SignatureVersion' => '',
      'Message' => \json_encode([
        'id' => '1',
        'name' => 'name',
        'url' => $this->getFixturePath('test_article', 'article.zip'),
        'metadataUrl' => $this->getFixturePath('test_article', 'metadata.json'),
        'articleJsonUrl' => $this->getFixturePath('test_article', 'article.json'),
        'tenantId' => '1',
        'brand' => 'test',
      ])
    ];
    $message = new Message($data);
    \Drupal::service('amazon_sns.message_dispatcher')->dispatch($message);

    $logs = $logger->cleanLogs();
    $this->assertEquals(RfcLogLevel::ERROR, $logs[0][0]);
    $this->assertEquals('Not processing Amazon SNS message with TopicArn @topicarn.', $logs[0][1]);
    $this->assertEquals('notatest:ecs-export-topic:test', $logs[0][2]['@topicarn']);
    $this->assertCount(0, SnsMessageEntity::loadMultiple());
  }

}
