<?php

namespace Drupal\Tests\ww_publish\Kernel;

use Drupal\Core\Test\TestDatabase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;

/**
 * Tests integration of static menu links.
 *
 * @group Menu
 */
abstract class WwPublishTestBase extends KernelTestBase {

  use ParagraphsTestBaseTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'ww_publish',
    'node',
    'paragraphs',
    'system',
    'field',
    'field_ui',
    'user',
    'entity_reference_revisions',
    'file',
    'text',
    'image',
    'media',
    'taxonomy',
    'link',
    'amazon_sns',
    'allowed_formats',
    'options'
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('media');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('taxonomy_term');
    $this->installSchema('node', ['node_access']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('ww_publish_sns_message');
    $this->installConfig(['field', 'file', 'image', 'media', 'node', 'system', 'ww_publish']);

    // Create a test-specific but real (no vfs://) temp directory so we can
    // assume that no other files are created during this test.
    $file_system = \Drupal::service('file_system');
    $test_db = new TestDatabase();
    $tmp_directory = $test_db->getTestSitePath() . '/tmp';
    $file_system->prepareDirectory($tmp_directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $this->setSetting('file_temp_path', $tmp_directory);
  }

  /**
   * Returns the relative path to a fixture resource.
   *
   * @param string $article
   *   The name of the test article fixture.
   * @param string $file_name
   *   The filename, article.json, metadata.json or article.zip.
   *
   * @return string
   *   The path to the fixture file.
   */
  protected function getFixturePath($article, $file_name) {
    return \Drupal::service("extension.list.module")->getPath('ww_publish') . '/tests/fixtures/' . $article . '/' . $file_name;
  }

}
