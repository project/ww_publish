CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 * Background information

INTRODUCTION
------------

The module enable to publish content from WoodWing Studio via Amazon Simple
Notification Service.

More info:

 * For a full description of the module, visit
   [the project page](https://www.drupal.org/project/ww_publish).

 * To submit bug reports and feature suggestions, or to track changes, visit
   [the issue page](https://www.drupal.org/project/issues/ww_publish).

REQUIREMENTS
------------

 * [Amazon Simple Notification Service](
   https://www.drupal.org/project/amazon_sns)

RECOMMENDED MODULES
-------------------

There are no recommended modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See the
   [docs](https://www.drupal.org/documentation/install/modules-themes/modules-8)
   for further information.

CONFIGURATION
-------------

The module have to be configured on several pages:

 1. Main settings: admin/config/services/ww-publish
 2. Templates: admin/config/services/ww-publish/templates
 3. Content type settings: admin/structure/types/manage/my_content_type
 4. Paragraphs: admin/structure/paragraphs_type (if the module paragraphs is
    used)

### Ad. 1.: Main settings

 * If the **WoodWing Studio URL** is filled, a link to the Article in the
   WoodWing Studio will be displayed in the SNS message edit form.
 * The ww_publish module needs to know in which content type the article should
   be published. In the setting **Content type field** have to be filled the
   name with the WoodWing Studio field, e.g.: "C_CS_DE_ARTICLETYPE".
 * The default user, which will be used in for the nodes and log messages have
   to be set in **WW User**.
 * In **WW User field** cann be filled a machine name of the field in the user
   entity (e.g.: "field_ww_user"), where will be saved the identification of the
   WoodWing user (e.g.: "Slejska, Antonin"). The Drupal field have to be a
   simple text field with maximal 1 value. The field
   (admin/config/people/accounts/fields/user.user.field_ww_user) have
   to contain the "WoodWing Studio identifier", e.g.:
   "WorkflowMetaData|Modifier".
 * Node author can be mapped from an ExtraMetaDataField, by specifying the
   identifier like: ExtraMetaData|FIELD_NAME.
 * It is possible to display error messages to the editors, if the **Error
   messages field** is filled, e.g.: "field_error_messages". The target field
   have to be a long text field with unlimited number of values.
 * Images from WoodWing Studio will be uploaded into the folder specified in
   **Path for the WW public folder**, e.g.: "ww".
 * When the ww_publish module is configured, the **debug mode** can be useful,
   but the debug mode should not be used on a live site.
 * SNS messages will now always be saved as content entities
   (admin/config/services/ww-publish/sns-messages). The response to Amazon will
   be sent just after saving the SNS message, given a compatible PHP mode like
   PHP-FPM is used. When messages are only imported during cron then they are
   just saved when received otherwise they are processed in a service destruct
   method to avoid race conditions when not providing a response to Amazon fast
   enough.
 * The SNS messages cann be automatically deleted, if there is a number in
   **Number of days, after which Amazon SNS messages are deleted.** The files
   are deleted maximal in two weeks on the Amazon server. The SNS messages are
   useless afterwards.
 * If the site uses the module "HTML title", then it is possible to **enable
   HTML title**.
 * Amazon SNS Arn Topic, to avoid having unauthorized content published in
   Drupal, the messages can be locked down to a specific Topic ARN.
 * A default text format can be specified to be used primarily in Text fields.
   This setting will be validated with the allowed formats for each field and
   select a valid one.

### Ad. 2.: Templates

Templates enable to define, how should be the content of a WoodWing Component or
its attribute published in Drupal. See a few simple examples of templates.

    Component "title-h2": "<h2>{{insert}}</h2>"
    Component "list-item": "<li>{{insert}}</li>"
    Component "body": "<p>{{insert}}</p>"
    Attribute "bold": "<strong>{{insert}}</strong>"
    Attribute "size": "<span style="font-size:{{size}}">{{insert}}</span>"
    Attribute "href": "<a href="{{href}}">{{insert}}</a>"

It is possible to use a few functions on the WoodWing content. See the examples:

```
toLower - e.g.: {{insert::toLower}} - Transforms the text to lower case.
toUpper - e.g.: {{insert::toUpper}} - Transforms the text to upper case.
replace - e.g. replace({"search":["  ","\n"],"replace":[" ","<br />"]})
 - Replaces an array of strings.
```

Component templates can be used between Paragraphs and Node fields. Content
properties are available as separate variables in the template. See this example
to access other variables:

```
<blockquote>
  <p>{{text}}</p>
  <footer>{{author}}, {{function}}</footer>
</blockquote>
```

To map a component to multiple Paragraph types add multiple comma separate names
in the identifier setting, e.g.: "body,intro".

### Ad. 3.: Content type settings

 * As **WoodWing Studio identifier for the title field** cann be used for
   example the component "title-h1".
 * If you use the module **scheduler**, then you have to define the components
   where are the dates for "publish_on" and "unpublish_on" fields.
 * In the **Article ID field** must be filled the machine name of the field, in
   which the WoodWing ID will be saved, e.g.: "field_ww_id". The field should be
   a plain text field with maximal one value.
 * If the site uses the module paragraphs, then the **Paragraphs field** have to
   be filled, e.g.: "field_paragraphs".
 * StoryID can be used to identify articles in newer Woodwing versions.

Fields, which should get content from WoodWing Studio, need to have the
**WoodWing Studio identifier** filled. See a few examples:

 * field_abstract: "intro"
 * field_title_image: "titleimagecreditcaption|image"
 * field_title_image_caption: "titleimagecreditcaption|caption"
 * field_title_image_credit: "titleimagecreditcaption|credit"

### Ad. 4.: Paragraphs

For every component should be created a separate paragraph type, mostly with
only one field. The field must have filled the "WoodWing Studio identifier". See
a few examples:

 * field_body: "body"
 * field_body_start: "body-start"
 * field_title_h3: "title-h3"

It is also possible to use a paragraph field in a paragraph, e.g.:

 * field_list_item_paragraphs: "unordered-list|main";  field_list_item:
   "list-item"
 * field_list_item_paragraphs: "ordered-list|main";  field_list_item:
   "list-item"
 * field_image_paragraphs: "slideshow"; field_image: "imagecreditcaption|image",
   field_caption: "imagecreditcaption|caption", field_credit:
   "imagecreditcaption|credit"

TROUBLESHOOTING
---------------

There are not any known issues. See:

 * [Issues](https://www.drupal.org/project/issues/ww_publish)
 * [Automated testing](https://www.drupal.org/node/3152484/qa)
 * [PAReview](http://pareview.net/r/101)

FAQ
---

There are no answers or issues.

MAINTAINERS
-----------

Current maintainers:

 * [Antonín Slejška](https://www.drupal.org/u/anton%C3%ADn-slej%C5%A1ka)

BACKGROUND INFORMATION
-----------

Manuals:

 * [The JSON Digital article format specification in Studio](
   https://helpcenter.woodwing.com/hc/en-us/articles/360040133912)
 * [Adding custom functionality to Content Station Aurora](
   https://helpcenter.woodwing.com/hc/en-us/articles/115005563966)
 * [Adding custom integrations to the Digital editor of Content Station Aurora](
   https://helpcenter.woodwing.com/hc/en-us/articles/360000268566)
 * [Configuring Content Station Aurora for publishing to a custom Publish
   Channel](https://helpcenter.woodwing.com/hc/en-us/articles/115005564026)
 * [Setting up Component Sets in Content Station Aurora](
   https://helpcenter.woodwing.com/hc/en-us/articles/360001244003)
