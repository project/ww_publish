<?php

namespace Drupal\ww_publish\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for ww_publish templates.
 */
interface TemplateEntityInterface extends ConfigEntityInterface {
}
