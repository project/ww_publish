<?php

namespace Drupal\ww_publish\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the template entity.
 *
 * @ConfigEntityType(
 *   id = "ww_publish_template",
 *   label = @Translation("WoodWing Studio template"),
 *   module = "ww_publish",
 *   config_prefix = "ww_publish_template",
 *   handlers = {
 *     "list_builder" = "Drupal\ww_publish\TemplateListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ww_publish\Form\TemplateForm",
 *       "edit" = "Drupal\ww_publish\Form\TemplateForm",
 *       "delete" = "Drupal\ww_publish\Form\TemplateDeleteForm",
 *     }
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "edit" = "/admin/config/services/ww-publish/templates/{ww_publish_template}",
 *     "delete" = "/admin/config/services/ww-publish/templates/{ww_publish_template}/delete",
 *     "collection" = "/admin/config/services/ww-publish/templates",
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "type",
 *     "content_type",
 *     "target",
 *     "template",
 *   }
 * )
 */
class TemplateEntity extends ConfigEntityBase implements TemplateEntityInterface {

  /**
   * Internal Drupal ID of the template entity.
   *
   * The ID
   *
   * @var string
   */
  public $id = NULL;

  /**
   * Name of the template entity.
   *
   * @var string
   */
  public $name = NULL;

  /**
   * Type of the template entity.
   *
   * 'component' type defines a template for a WoodWing Studio component.
   * 'attribute' type defines a template for an attribute of WoodWing Studio
   * components.
   *
   * @var string
   */
  public $type = 'component';

  /**
   * Content type.
   *
   * Defines for which content type the template will be used.
   *
   * @var string
   */
  public $content_type = 'all';

  /**
   * Target of the template.
   *
   * Defines for which component, attribute, etc. the template will be used.
   *
   * @var string
   */
  public $target = NULL;

  /**
   * Code of the template.
   *
   * @var string
   */
  public $template = NULL;

}
