<?php

namespace Drupal\ww_publish\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the SNS message entity.
 *
 * @ContentEntityType(
 *   id = "ww_publish_sns_message",
 *   label = @Translation("SNS message"),
 *   handlers = {
 *     "list_builder" = "Drupal\ww_publish\SnsMessageListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ww_publish\Form\SnsMessageForm",
 *       "edit" = "Drupal\ww_publish\Form\SnsMessageForm",
 *       "delete" = "Drupal\ww_publish\Form\SnsMessageDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ww_publish_sns_message",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/services/ww-publish/sns-messages/add",
 *     "edit-form" = "/admin/config/services/ww-publish/sns-messages/{ww_publish_sns_message}",
 *     "delete-form" = "/admin/config/services/ww-publish/sns-messages/{ww_publish_sns_message}/delete",
 *     "collection" = "/admin/config/services/ww-publish/sns-messages",
 *   }
 * )
 */
class SnsMessageEntity extends ContentEntityBase implements SnsMessageEntityInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 100)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ]);

    $fields['url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('URL'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => 3,
      ]);

    $fields['metadata_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Metadata URL'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => 4,
      ]);

    $fields['article_json_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Article JSON URL'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'uri',
        'weight' => 5,
      ]);

    $fields['tenant_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Tenant ID'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 6,
      ]);;

    $fields['brand'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Brand'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 8,
      ]);

    $fields['status'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('Status'))
      ->setDefaultValue(0)
      ->setSetting('unsigned', TRUE)
      ->setSetting('allowed_values', [
        static::NEW => t('New'),
        static::IMPORTED => t('Imported'),
        static::FAILED => t('Failed'),
        static::SKIPPED => t('Skipped'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 10,
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getZipUrl() {
    return $this->get('url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataUrl() {
    return $this->get('metadata_url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getArticleJsonUrl() {
    return $this->get('article_json_url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTenantId() {
    return $this->get('tenant_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getBrand() {
    return $this->get('brand')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isExecuted() {
    return $this->get('status')->value != static::NEW;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

}
