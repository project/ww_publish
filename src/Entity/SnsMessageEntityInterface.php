<?php

namespace Drupal\ww_publish\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for ww_publish SNS messages.
 */
interface SnsMessageEntityInterface extends ContentEntityInterface {

  /**
   * New status.
   */
  const NEW = 0;

  /**
   * Successfully imported status.
   */
  const IMPORTED = 1;

  /**
   * FAiled import status.
   */
  const FAILED = 2;

  /**
   * Skipped status.
   */
  const SKIPPED = 3;

  /**
   * Returns the URL to the ZIP file.
   *
   * @return string
   *   The ZIP file URL.
   */
  public function getZipUrl();

  /**
   * Returns the URL to the metadata.json file.
   *
   * @return string
   *   The metadata.json file URL.
   */
  public function getMetadataUrl();

  /**
   * Returns the URL to the article.json file.
   *
   * @return string
   *   The article.json file URL.
   */
  public function getArticleJsonUrl();

  /**
   * Returns the Tenant ID.
   *
   * @return string
   *   The Tenant ID.
   */
  public function getTenantId();

  /**
   * Returns the brand.
   *
   * @return string
   *   The brand.
   */
  public function getBrand();

  /**
   * Returns the created timestamp.
   *
   * @return int
   *   Timestamp when this entity was created.
   */
  public function getCreated();

  /**
   * Returns whether the entity was already imported.
   *
   * @return bool
   *   True if the message was already imported.
   */
  public function isExecuted();

  /**
   * Returns whether the entity was already imported.
   *
   * @return bool
   *   True if the message was already imported.
   */
  public function getStatus();

}
