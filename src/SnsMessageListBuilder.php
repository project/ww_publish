<?php

namespace Drupal\ww_publish;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a list of SNS message entities.
 */
class SnsMessageListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // Prepare the table header.
    $header = [];
    $header['id'] = $this->t('Id');
    $header['name'] = $this->t('Name of the SNS message');
    $header['url'] = $this->t('URL of the SNS message');
    $header['status'] = $this->t('Status');
    $header['created'] = $this->t('Created');

    // Return the table header.
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    // Prepare the table row for the endpoint.
    /** @var \Drupal\ww_publish\Entity\SnsMessageEntityInterface $entity */
    $row = [];
    $row['id'] = $entity->id();
    $row['name'] = $entity->label();
    $row['url'] = $entity->getZipUrl();
    $row['status']['data'] = $entity->get('status')->view(['type' => 'list_default', 'label' => 'hidden'])[0] ?? [];
    $row['created'] = \Drupal::service('date.formatter')->format($entity->getCreated(), 'short');

    // Return the table row.
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    // Prepare the table row operations for the SNS message.
    $operations = parent::getDefaultOperations($entity);
    if ($entity->hasLinkTemplate('edit')) {
      $operations['edit'] = [
        'title' => $this->t('Edit SNS message'),
        'weight' => 20,
        'url' => $entity->toUrl('edit'),
      ];
    }
    if ($entity->hasLinkTemplate('delete')) {
      $operations['delete'] = [
        'title' => $this->t('Delete SNS message'),
        'weight' => 40,
        'url' => $entity->toUrl('delete'),
      ];
    }

    // Return the table row operations.
    return $operations;
  }

}
