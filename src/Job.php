<?php

namespace Drupal\ww_publish;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\ww_publish\Entity\SnsMessageEntity;
use Drupal\ww_publish\Entity\SnsMessageEntityInterface;

class Job {

  /**
   * Configuration of the ww_publish module.
   *
   * @var \Drupal\Core\Config\Config
   */
  private $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager, LockBackendInterface $lock, FileSystemInterface  $file_system) {
    $this->config = $config_factory->get('ww_publish.settings');
    $this->logger = $logger_factory->get('ww_publish');
    $this->entityTypeManager = $entity_type_manager;
    $this->lock = $lock;
    $this->fileSystem = $file_system;
  }

  /**
   * Create or update node.
   *
   * @param int $message_id
   *   The message entity ID.
   *
   * @return void
   *
   * @throws \InvalidArgumentException
   *   Thrown if an invalid message is passed in.
   */
  public function publishArticle($message_id) : void {

    $lock_id = 'ww_publish:publish:' . $message_id;
    if (!$this->lock->acquire($lock_id)) {
      // Failed to acquire the lock for this message, another request
      // might process it right now, ignore.
      return;
    }

    try {
      // Load the unchanged entity to ensure that it is returned based on the
      // current database state and not an outdated static cache.
      /** @var \Drupal\ww_publish\Entity\SnsMessageEntityInterface $eventMessage */
      $eventMessage = $this->entityTypeManager->getStorage('ww_publish_sns_message')->loadUnchanged($message_id);

      // If the message is already executed then another request already did the
      // import, abort early.
      if ($eventMessage->isExecuted()) {
        return;
      }

      // Prepare the status as failed, if an exception is thrown, this value
      // will be saved.
      $eventMessage->set('status', SnsMessageEntityInterface::FAILED);

      // Get article metadata.
      $message = new Message($eventMessage, $this->config, $this->logger);
      $metadata = $message->getArticleMetadata();
      $articleData = $message->getArticleData();
      if (!$metadata || !$articleData) {
        throw new \InvalidArgumentException('Failed to fetch article data or metadata.');
      }

      $this->logger->setCurrentUser($metadata->getAuthor());
      if ($this->config->get('debug_mode'))
        $this->logger->debug('Article Metadata: <pre><code>@metadata</code></pre>', ['@metadata' => print_r($metadata->getMetadata(), TRUE)]);

      if ($metadata->validate()) {
        if ($this->config->get('debug_mode'))
          $this->logger->debug('Article: <pre><code>@article_data</code></pre>', ['@article_data' => print_r($articleData, TRUE)]);
      } else {
        throw new \InvalidArgumentException('The metadata could not be validated.');
      }

      $preparedFiles = $message->prepareArticleFiles();
      if ($this->config->get('debug_mode'))
        $this->logger->debug('Prepared files: <pre><code>@files</code></pre>', ['@files' => print_r($preparedFiles, TRUE)]);

      if ($preparedFiles === FALSE) {
        throw new \InvalidArgumentException('Failed to prepare images: ' . \implode(',', $message->getErrors()));
      }

      $article = new Article($message, $this->config, $this->logger);
      $eventMessage->set('status', $article->publish());

      foreach ($message->getFilesToRemoveAfterPublish() as $fileDirName) {
        $this->fileSystem->deleteRecursive($fileDirName);
      }

    }
    finally {
      $eventMessage->save();
      $this->lock->release($lock_id);
    }

  }

}
