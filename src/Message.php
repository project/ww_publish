<?php

namespace Drupal\ww_publish;

use Drupal\Core\Archiver\ArchiverException;
use Drupal\Core\Archiver\Zip as DrupalZip;
use Drupal\Core\Config\Config;
use Drupal\ww_publish\Entity\SnsMessageEntityInterface;
use Psr\Log\LoggerInterface;

class Message {

  /**
   * SNS Message.
   *
   * @var \Drupal\ww_publish\Entity\SnsMessageEntityInterface
   */
  protected $message;

  /**
   * Error messages.
   *
   * @var array
   */
  protected $errors = array();

  /**
   * The metadata object.
   *
   * @var \Drupal\ww_publish\Metadata|null
   */
  protected $metadata;

  /**
   * The article data.
   *
   * @var object
   */
  protected $articleData;

  /**
   * List of files to remove.
   *
   * @var string[]
   */
  protected $filesToRemoveAfterPublish = [];

  /**
   * list of images that can be imported, keyed by the ID.
   *
   * @var string[]
   */
  protected $images = [];

  /**
   * Constructor.
   *
   * @param \Drupal\ww_publish\Entity\SnsMessageEntityInterface $message
   *   SNS Message.
   * @param \Drupal\Core\Config\Config $config
   *   The 'ww_publish.settings' config.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger.
   */
  public function __construct(SnsMessageEntityInterface $message, Config $config, LoggerInterface $logger) {
    $this->message = $message;
    $this->config = $config;
    $this->logger = $logger;
  }

  public function getArticleID() {
    return $this->message->id();
  }

  public function getArticleName() {
    return $this->message->label();
  }

  public function getArticleZipName() {
     return basename($this->message->getZipUrl());
  }

  /**
   * Prepares article files.
   *
   * @return false|string[]
   *   List of extracted images.
   */
  public function prepareArticleFiles() {
    $fileSystem = \Drupal::service('file_system');
    // Check tmp dir. Create if not exist.
    $tempDir = \Drupal::service('file_system')->getTempDirectory();
    if (!file_exists($tempDir)) {
      if (!$fileSystem->mkdir($tempDir)) {
        $this->errors[] = "failed to create tmp dir [$tempDir], no further processing";
        return FALSE;
      }
    }
    // Save to disk.
    $zipName = $this->getArticleZipName();
    // Store the ZIP file in the temporary folder.
    $zipNamePath = $tempDir . DIRECTORY_SEPARATOR . $zipName;
    $this->downloadArticleZipToPath($zipNamePath);

    // Prepare the drupal-side.
    $articleDirName = $this->getArticleID() . '-' . $zipName;
    $articleDir = $tempDir . DIRECTORY_SEPARATOR . $articleDirName;
    if (file_exists($articleDir)) {
      $fileSystem->deleteRecursive($articleDir);
    }

    // Unzip the ZIP file to the folder.
    try {
      $zip = new DrupalZip($zipNamePath);
      $zip->extract($articleDir);
    } catch (ArchiverException $e) {
      $this->errors[] = $e->getMessage();
      return FALSE;
    }

    if (file_exists($articleDir . '/img')) {
      $this->images = $this->getImagesFromPath($articleDir . '/img');
    }

    $this->filesToRemoveAfterPublish = [$zipNamePath, $articleDir];
    return $this->images;
  }

  /**
   * Returns the image for the given ID.
   *
   * @param int $id
   *   The image ID.
   *
   * @return string|null
   */
  public function getImage($id) {
    return $this->images[$id] ?? NULL;
  }

  /**
   * @return string[]
   */
  public function getFilesToRemoveAfterPublish(): array {
    return $this->filesToRemoveAfterPublish;
  }

  /**
   * Download the ZIP file.
   *
   * Get the ZIP file from the path specified in the message and download it to
   * the specified path.
   *
   * @param string $zipPath
   *   Path for the ZIP file.
   *
   * @return bool|false|string|null
   */
  public function downloadArticleZipToPath($zipPath) {
    $zipData = file_get_contents($this->message->getZipUrl());
    if (!$zipData) {
      $this->errors[] = "Problem loading article zipped data, no data.";
    }
    file_put_contents($zipPath, $zipData);
    return $zipData;
  }

  private function getImagesFromPath($imageFolder) {
    $imagesPath = [];
    $dh = opendir($imageFolder);
    if ($dh !== false) {
      $filename = readdir($dh);
      while ($filename !== false) {
        if ($filename != '.' && $filename != '..' && $filename !== false) {
          $filenameWithoutType = substr($filename, 0, strpos($filename, '.'));
          $imagesPath[$filenameWithoutType] = $imageFolder .'/'.  $filename ;
        }
        $filename = readdir($dh);
      }
    }
    closedir($dh);

    return $imagesPath;
  }

  /**
   * Returns the article metadata.
   *
   * @return \Drupal\ww_publish\Metadata|null
   *   Either a Metadata object or NULL if it could not be loaded.
   */
  public function getArticleMetadata() {
    if (!$this->metadata) {
      $metadata = file_get_contents($this->message->getMetadataUrl());
      if ($metadata) {
        $this->metadata = new Metadata(json_decode($metadata), $this->config, $this->logger);
      }
    }
    return $this->metadata;
  }

  public function getArticleData() {
    if (!$this->articleData) {
      $articleJSON = file_get_contents($this->message->getArticleJsonUrl());
      if ($articleJSON) {
        $this->articleData = json_decode($articleJSON);
      }
    }
    return $this->articleData;
  }

  public function getErrors () {
    if ($this->errors) {
      return $this->errors;
    }
    return false;
  }

}
