<?php

namespace Drupal\ww_publish;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Provides methods for Drupal fields used to publish WW articles.
 */
trait FieldTrait {

  /**
   * Returns an allowed text format for the specified field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   A field definition object.
   *
   * @return string|null
   *   The name of the allowed format to use for a field.
   */
  private function getTextFormat(FieldDefinitionInterface $field_definition) {
    $default_test_format = \Drupal::config('ww_publish.settings')->get('default_text_format');
    if ($field_definition instanceof FieldConfigInterface) {
      $allowed_formats_setting = $field_definition->getThirdPartySettings('allowed_formats');
      $allowed_formats_setting = isset($allowed_formats_setting['allowed_formats']) && \is_array($allowed_formats_setting['allowed_formats']) ? $allowed_formats_setting['allowed_formats'] : $allowed_formats_setting;
      $allowed_formats_setting = array_filter($allowed_formats_setting);
      if ($allowed_formats_setting && !isset($allowed_formats_setting[$default_test_format])) {
        $default_test_format = \reset($allowed_formats_setting);
      }
    }
    return $default_test_format;
  }

  /**
   * Get attribute content.
   *
   * @param Object $text
   *   The text object with or without attributes (formatting).
   * @param bool $escape
   *   (optional) Whether to escape the content.
   *
   * @return string
   */
  private function getAtributeContent($text, bool $escape = FALSE): string {
    if (is_string($text)) {
      return $escape ? Html::escape($text) : $text;
    }
    $attributeContent = $text->insert ?? '';

    $attributeContent = $escape ? Html::escape($attributeContent) : $attributeContent;

    if (property_exists($text, 'attributes')) {
      $template = \Drupal::service('ww_publish.template');
      foreach ($text->attributes as $attribute => $attributeValue) {
        // Skip applying the template if the value is null, as the attribute
        // should not be applied then.
        if ($attributeValue === NULL) {
          continue;
        }
        if ($attribute === 'comment') {
          continue;
        }
        // Attribute templates are expected to exist or structure is lost,
        // call apply() directly to trigger a warning log message about
        // missing templates.
        $attributeContent = $template->apply($attribute, 'attribute', [
          'insert' => $attributeContent,
          $attribute => $attributeValue,
        ]) ?: $attributeContent;
      }
    }
    return $attributeContent;
  }

  /**
   * Returns shortened value if longer than the field maximum length.
   *
   * @param string $value
   *   Value to be shortened.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array|null $messages_field
   *   The messages field to add errors to.
   *
   * @return string
   *   The possibly shortened value.
   */
  protected function shortenValueToFieldMaxLength($value, FieldDefinitionInterface $field_definition, array &$messages_field = NULL) {
    $settings = $field_definition->getFieldStorageDefinition()->getSettings();

    if (isset($settings['max_length']) && is_int($settings['max_length']) && strlen($value) > $settings['max_length']) {
      $value = substr($value, 0, $settings['max_length']);

      // Log the error message, that the value has to be shortened.
      $this->logger->error('The value which should be saved in the field <strong>@field_name</strong> is too long.<br> Maximum length: <strong>@max_length<br> Shortened value: <i>@value</i>', [
          '@field_name' => $field_definition->getName(),
          '@max_length' => $settings['max_length'],
          '@value' => $value,
        ]
      );
      if ($messages_field !== NULL) {
        $messages_field[]['value'] = t("The value which should be saved in the field @field_name is too long.\nMaximum length: @max_length\nShortened value: @value", [
            '@field_name' => $field_definition->getName(),
            '@max_length' => $settings['max_length'],
            '@value' => $value,
          ]
        );
      }
    }

    return $value;
  }

}
