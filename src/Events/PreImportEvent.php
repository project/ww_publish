<?php

namespace Drupal\ww_publish\Events;

use Drupal\node\NodeInterface;
use Drupal\ww_publish\Message;
use Drupal\Component\EventDispatcher\Event;

/**
 * Allows to alter which node is updated or skip the import.
 */
class PreImportEvent extends Event {

  /**
   * The existing node, if found.
   *
   * @var \Drupal\node\NodeInterface|null
   */
  protected $node;

  /**
   * The SNS message.
   *
   * @var \Drupal\ww_publish\Message
   */
  protected $message;

  /**
   * Whether the import should be skipped.
   *
   * @var bool
   */
  protected $skip = FALSE;

  /**
   * PreImportEvent constructor.
   *
   * @param \Drupal\ww_publish\Message $message
   *   The SNS message.
   * @param \Drupal\node\NodeInterface|null $node
   *   The existing node, if found.
   */
  public function __construct(Message $message, NodeInterface $node = NULL) {
    $this->message = $message;
    $this->node = $node;
  }

  /**
   * Returns the existing node, if found.
   *
   * @return \Drupal\node\NodeInterface
   *   The existing node to update.
   */
  public function getNode(): ?NodeInterface {
    return $this->node;
  }

  /**
   * Sets the node that should be updated.
   *
   * @param \Drupal\node\NodeInterface|null $node
   *   The node to update, NULL to create a new node instead.
   */
  public function setNode(NodeInterface $node = NULL) {
    $this->node = $node;
  }

  /**
   * Returns the SNS message.
   *
   * @return \Drupal\ww_publish\Message
   *   The SNS message.
   */
  public function getMessage(): Message {
    return $this->message;
  }

  /**
   * Returns whether the import should be skipped.
   *
   * @return bool
   *   TRUE if the import should be skipped.
   */
  public function isSkip(): bool {
    return $this->skip;
  }

  /**
   * Sets whether the import should be skipped.
   *
   * @param bool $skip
   *   TRUE if the import should be skipped, FALSE if not.
   */
  public function setSkip(bool $skip) {
    $this->skip = $skip;
  }

}
