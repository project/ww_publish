<?php

namespace Drupal\ww_publish\Events;

use Drupal\paragraphs\ParagraphInterface;
use Drupal\ww_publish\Message;
use Drupal\Component\EventDispatcher\Event;

/**
 * Allows to alter the paragraph that is created for a given content component.
 */
class ParagraphFieldEvent extends Event {

  /**
   * The SNS message.
   *
   * @var \Drupal\ww_publish\Message
   */
  protected $message;

  /**
   * The content component.
   *
   * @var object
   */
  protected $wwComponent;

  /**
   * The created paragraph, if any.
   *
   * @var \Drupal\paragraphs\ParagraphInterface|null
   */
  protected $paragraph = NULL;

  /**
   * ParagraphFieldEvent constructor.
   *
   * @param \Drupal\ww_publish\Message $message
   *   The SNS message.
   * @param object $wwComponent
   *   The content component.
   * @param \Drupal\paragraphs\ParagraphInterface|null $paragraph
   *   The created paragraph, if any.
   */
  public function __construct(Message $message, $wwComponent, ParagraphInterface $paragraph = NULL) {
    $this->message  = $message;
    $this->paragraph = $paragraph;
    $this->wwComponent = $wwComponent;
  }

  /**
   * Returns the SNS message.
   *
   * @return \Drupal\ww_publish\Message
   *   The SNS message.
   */
  public function getMessage(): Message {
    return $this->message;
  }

  /**
   * Returns the content component.
   *
   * @return object
   *   The content component.
   */
  public function getWwComponent() {
    return $this->wwComponent;
  }

  /**
   * Returns the prepared paragraph.
   *
   * @return \Drupal\paragraphs\ParagraphInterface|null
   *   The paragraph if one could be prepared.
   */
  public function getParagraph(): ?ParagraphInterface {
    return $this->paragraph;
  }

  /**
   * Set or unset the created paragraph.
   *
   * @param \Drupal\paragraphs\ParagraphInterface|null $paragraph
   *   A new paragraph or NULL to not create a paragraph for the component.
   */
  public function setParagraph(ParagraphInterface $paragraph = NULL) {
    $this->paragraph = $paragraph;
  }

}
