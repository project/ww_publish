<?php

namespace Drupal\ww_publish\Events;

use Drupal\node\NodeInterface;
use Drupal\ww_publish\Message;
use Drupal\Component\EventDispatcher\Event;

/**
 * Allows to alter the created node.
 */
class ArticleFieldsEvent extends Event {

  /**
   * The SNS messge.
   *
   * @var \Drupal\ww_publish\Message
   */
  protected $message;

  /**
   * The prepared node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * ArticleFieldsEvent constructor.
   *
   * @param \Drupal\ww_publish\Message $message
   *   The SNS message.
   * @param \Drupal\node\NodeInterface $node
   *   The prepared node.
   */
  public function __construct(Message $message, NodeInterface $node) {
    $this->message = $message;
    $this->node = $node;
  }

  /**
   * Returns the prepared node.
   *
   * @return \Drupal\node\NodeInterface
   *   The prepared node.
   */
  public function getNode(): NodeInterface {
    return $this->node;
  }

  /**
   * Returns the SNS message.
   *
   * @return \Drupal\ww_publish\Message
   *   The SNS message.
   */
  public function getMessage(): Message {
    return $this->message;
  }

}
