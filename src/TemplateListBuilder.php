<?php

namespace Drupal\ww_publish;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a list of template entities.
 */
class TemplateListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // Prepare the table header.
    $header = [];
    $header['id'] = $this->t('Id');
    $header['name'] = $this->t('Name of the template');
    $header['type'] = $this->t('Type of the template');
    $header['content_type'] = $this->t('Content type');
    $header['target'] = $this->t('target');

    // Return the table header.
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    // Prepare the table row for the endpoint.
    $row = [];
    $row['id'] = $entity->id();
    $row['name'] = $entity->name;
    $row['type'] = $entity->type;
    $row['content_type'] = $entity->content_type;
    $row['target'] = $entity->target;

    // Return the table row.
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    // Prepare the table row operations for the template.
    $operations = parent::getDefaultOperations($entity);
    if ($entity->hasLinkTemplate('edit')) {
      $operations['edit'] = [
        'title' => $this->t('Edit template'),
        'weight' => 20,
        'url' => $entity->toUrl('edit'),
      ];
    }
    if ($entity->hasLinkTemplate('delete')) {
      $operations['delete'] = [
        'title' => $this->t('Delete template'),
        'weight' => 40,
        'url' => $entity->toUrl('delete'),
      ];
    }

    // Return the table row operations.
    return $operations;
  }

}
