<?php

namespace Drupal\ww_publish\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Template entity delete form for ww_publish.
 */
class SnsMessageDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.ww_publish_sns_message');
  }

}
