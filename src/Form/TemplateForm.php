<?php

namespace Drupal\ww_publish\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;

/**
 * Template entity form for ww_publish.
 */
class TemplateForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // Prepare the form.
    $form = parent::form($form, $form_state);

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the template'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $this->entity->name,
      '#description' => $this->t('The name is used to generate the ID. Do not use solely the name of the WoodWing Studio component or attribute.'),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#disabled' => !$this->entity->isNew(),
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['name'],
      ],
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of the template'),
      '#required' => TRUE,
      '#default_value' => $this->entity->type,
      '#options' => [
        'component' => 'WoodWing Studio component',
        'attribute' => 'WoodWing Studio component\'s attribute'
        ],
    ];

    $form['content_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content type'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $this->entity->content_type,
      '#description' => $this->t('The template can be used for all content types or just for one content type. In this case use machine name of the content type.'),
    ];

    $form['target'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Target'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $this->entity->target,
      '#description' => $this->t('Specify the identifier of the component or the name of the attribute.'),
    ];

    $form['template'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Template'),
      '#required' => TRUE,
      '#default_value' => $this->entity->template,
      '#description' => $this->t(
        'Define the HTML code of the template. In the template you should use the parameters, from the WoodWing Components, e.g.: {{insert}}, {{href}}. The parameters can be transformed with the following funtions:<br>' .
        'toLower - e.g.: {{insert::toLower}} - Transforms the text to lower case.<br>' .
        'toUpper - e.g.: {{insert::toUpper}} - Transforms the text to upper case.<br>' .
        'replace - e.g. replace({"search":["&nbsp;&nbsp;","\n"],"replace":["&nbsp;","&lt;br /&gt;"]}) - Replaces an array of strings.)'
      ),
    ];

    // Return the form.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Get all content types.
    $types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    // Retrieve $content_type (which, being required, is surely not blank).
    $content_type = $form_state->getValue('content_type');

    // Test if the content type exists.
    if ($types[$content_type] === NULL && $content_type != 'all') {
      $form_state->setErrorByName('content_type', $this->t('The content type does not exist.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Save the template and display the status message.
    try {
      $this->entity->save();
    }
    catch (EntityStorageException $exception) {
      $this->logger('ww_publish')->error('The template %id was not saved.', ['%id' => $this->entity->id()]);
      $this->messenger()->addError($this->t('The template %id was not saved.', ['%id' => $this->entity->id()]));
    }
    // Redirect to the proper url.
    $form_state->setRedirect('entity.ww_publish_template');
  }

  /**
   * Checks if the template exists.
   *
   * @param string $id
   *   Machine name (ID) of the template.
   *
   * @return bool
   *   Returns true, if the template exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists($id) {
    $storage = $this->entityTypeManager->getStorage('ww_publish_template');
    return (bool) $storage->getQuery()->condition('id', $id)->execute();
  }

}
