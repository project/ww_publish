<?php

namespace Drupal\ww_publish\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\ww_publish\Metadata;

/**
 * SNS message entity form for ww_publish.
 */
class SnsMessageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $links = [];
    $config = $this->config('ww_publish.settings');
    if ($config->get('studio_url') && !$this->entity->isNew()) {
      $links[] = [
        'title' => $this->t('Edit the article in WW Studio'),
        'url' => Url::fromUri($config->get('studio_url') . '#/article/i/' . $this->entity->id())
      ];
    }
    if (!$this->entity->isNew() && $node = $this->findNode()) {
      $links[] = [
        'title' => $this->t('Edit the article in Drupal'),
        'url' => $node->toUrl('edit-form'),
      ];
      $links[] = [
        'title' => $this->t('Display the article'),
        'url' => $node->toUrl(),
      ];
    }
    if ($links) {
      $form['intro'] = [
        '#theme' => 'links',
        '#links' => $links,
        '#heading' => ['text' => $this->t("Article's links:")],
      ];
    }
    $form['#attached']['library'][] = 'core/drupal.form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    // Redirect to the proper url.
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

  /**
   * Checks if the SNS message exists.
   *
   * @param string $id
   *   Machine name (ID) of the SNS message.
   *
   * @return bool
   *   Returns true, if the SNS message exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists($id) {
    $storage = $this->entityTypeManager->getStorage('ww_publish_sns_message');
    return (bool) $storage->getQuery()->condition('id', $id)->execute();
  }

  /**
   * Returns the existing node for this article if existing.
   *
   * @return \Drupal\node\NodeInterface|null
   *   The node or NULL
   */
  protected function findNode(): ?NodeInterface {
    // Find all Article ID fields from all Node Types.
    $article_fields = [];

    // @todo Refactor fetching of data to avoid duplicating this logic.
    $storyid = NULL;
    $metadata_json = file_get_contents($this->entity->getMetadataUrl());
    if ($metadata_json) {
      $metadata = new Metadata(json_decode($metadata_json), $this->config('ww_publish.settings'), $this->logger('ww_publish'));
      $storyid = $metadata->getMetadataByIdentifier('BasicMetaData', 'StoryId');
    }


    foreach (NodeType::loadMultiple() as $node_type) {
      if ($article_id_field = $node_type->getThirdPartySetting('ww_publish', 'article_id_field')) {
        $article_fields[$article_id_field] = $article_id_field;
      }
      if ($article_storyid_field = $node_type->getThirdPartySetting('ww_publish', 'article_storyid_field')) {
        $article_fields[$article_storyid_field] = $article_storyid_field;
      }
    }

    $nodes = [];
    // Find the node.
    if (!empty($article_fields)) {
      // If there are no article fields configured don't run any query.
      $query = $this->entityTypeManager->getStorage('node')->getQuery('OR');
      $query->accessCheck(TRUE);
      foreach ($article_fields as $article_field) {
        $query->condition($article_field, $this->entity->id());
        if ($storyid) {
          $query->condition($article_field, $storyid);
        }
      }
      $nodes = $query->execute();
    }

    return count($nodes) === 1 ? Node::load(reset($nodes)) : NULL;
  }

}
