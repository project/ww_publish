<?php

namespace Drupal\ww_publish\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures WoodWing Studio settings for this site.
 */
class WwPublishSettingsForm extends ConfigFormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * WwPublishSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ww_publish_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ww_publish.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ww_publish.settings');

    // General form.
    $form['#attributes']['autocomplete'] = 'off';

    $form['woodwing'] = [
      '#type' => 'details',
      '#title' => 'WoodWing Studio',
      '#open' => TRUE,
    ];

    $form['woodwing']['studio_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WoodWing Studio URL'),
      '#description' => $this->t('E.g.: http://wwe10.my-site.org/Aurora/'),
      '#default_value' => $config->get('studio_url'),
    ];

    $form['woodwing']['content_type_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content type field'),
      '#description' => $this->t('WoodWing Studio metadata field name which contains the machine name of the Drupal content type, in which the nodes should be created/updated.'),
      '#default_value' => $config->get('content_type_field'),
      '#required' => TRUE,
    ];

    $form['woodwing']['ww_user'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('WW User'),
      '#description' => $this->t('This user will be used to save WW contents in Drupal.'),
      '#default_value' => User::load($config->get('ww_user') ?: 1),
      '#target_type' => 'user',
      '#selection_settings' => ['include_anonymous' => FALSE],
      '#required' => TRUE,
    ];

    $form['woodwing']['ww_user_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WW User field'),
      '#maxlength' => 255,
      '#default_value' => $config->get('ww_user_field'),
      '#description' => $this->t('Field in the user entity used to identify the WoodWing user. E.g.: field_ww_user.'),
    ];

    $form['woodwing']['error_messages_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error messages field'),
      '#maxlength' => 255,
      '#default_value' => $config->get('error_messages_field'),
      '#description' => $this->t('Field in the node entity used to save error messages, which will be displayed to the editors. The field should have the type "long plain text" and unlimited values.'),
    ];

    $form['woodwing']['ww_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path for the WW public folder'),
      '#maxlength' => 255,
      '#default_value' => $config->get('ww_path'),
      '#description' => $this->t('If the path in the file system does not exists, then will be the directory created.'),
      '#required' => TRUE,
    ];

    $form['woodwing']['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => t('Activate the debug mode.'),
      '#description' => $this->t('Do not use the debug mode on the live site.'),
      '#default_value' => $config->get('debug_mode'),
    ];

    $form['woodwing']['cron_mode'] = [
      '#type' => 'checkbox',
      '#title' => t('Only import messages during cron runs'),
      '#description' => $this->t('If this mode is enabled than messages are only imported during cron runs. If the server uses an approach that allows to run PHP after returning a response (e.g. PHP-FPM) then this option can also be disabled.'),
      '#default_value' => $config->get('cron_mode'),
    ];

    $form['woodwing']['sns_delete'] = [
      '#type' => 'number',
      '#title' => t('Number of days, after which Amazon SNS messages are deleted.'),
      '#description' => $this->t('0 = the messages will never be deleted.'),
      '#default_value' => $config->get('sns_delete'),
    ];

    $form['woodwing']['html_title'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable HTML title.'),
      '#description' => $this->t('The module <a href="https://www.drupal.org/project/html_title">HTML Title</a> have to be installed and enabled!'),
      '#default_value' => $config->get('html_title'),
    ];
    $options = [];
    $text_formats = $this->entityTypeManager->getStorage('filter_format')->loadByProperties(['status' => TRUE]);
    foreach ($text_formats as $format) {
      $options[$format->id()] = $format->label();
    }
    $form['woodwing']['default_text_format'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Default text format'),
      '#description' => t("The used text format when creating Paragraphs with text."),
      '#options' => $options,
      '#default_value' => $config->get('default_text_format'),
    ];
    $topic_arn_default = '';
    if ($config->get('topic_arn')) {
      $topic_arn_default = \implode("\n", $config->get('topic_arn'));
    }
    $form['woodwing']['topic_arn'] = [
      '#type' => 'textarea',
      '#title' => $this->t('TopicArn'),
      '#default_value' => $topic_arn_default,
      '#description' => $this->t('A list of the ARN Topics accepted to send messages, one per line. The provided values must only match the beginning of the string, a recommendation is to leave out the timestamp at the end of each ARN Topic to make it easier to handle multiple publication channels from the same WoodWing Studio system.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $path = trim($form_state->getValue('ww_path'));
    $config = $this->config('ww_publish.settings');
    $config->set('studio_url', trim($form_state->getValue('studio_url')));
    $config->set('content_type_field', trim($form_state->getValue('content_type_field')));
    $config->set('ww_user', trim($form_state->getValue('ww_user')));
    $config->set('ww_user_field', trim($form_state->getValue('ww_user_field')));
    $config->set('error_messages_field', trim($form_state->getValue('error_messages_field')));
    $config->set('ww_path', $path);
    $config->set('debug_mode', $form_state->getValue('debug_mode'));
    $config->set('cron_mode', $form_state->getValue('cron_mode'));
    $config->set('sns_delete', $form_state->getValue('sns_delete'));
    $config->set('html_title', $form_state->getValue('html_title'));
    $config->set('default_text_format', $form_state->getValue('default_text_format'));
    $topic_arns = explode("\n", str_replace("\r\n", "\n", $form_state->getValue('topic_arn')));
    $topic_arns = array_filter(array_map('trim', $topic_arns));
    $config->set('topic_arn', $topic_arns);
    $config->save();

    // Create the directory if it does not exist.
    $directory_path = 'public://' . $path;
    $this->fileSystem->prepareDirectory($directory_path, FileSystemInterface::CREATE_DIRECTORY);

    parent::submitForm($form, $form_state);
  }

}
