<?php

namespace Drupal\ww_publish;

use Drupal\Core\Field\FieldDefinitionInterface;

trait MediaFieldTrait {

  /**
   * Returns the media target bundle.
   *
   * @return string|null
   *   The name of the media target bundle if this is a media reference with a
   *   supported target type.
   */
  private function getMediaTargetBundle(FieldDefinitionInterface $field_definition) {
    $bundle = NULL;
    if (
      $field_definition->getSetting('handler') === 'default:media' &&
      $field_definition->getSetting('handler_settings') &&
      count($field_definition->getSetting('handler_settings')['target_bundles']) === 1
    ) {
      $bundle = reset($field_definition->getSetting('handler_settings')['target_bundles']);
    }
    return $bundle;
  }

  /**
   * Returns the media
   *
   * @param string $media_type
   *   The media type.
   * @param Object $image_content
   *   The image content component.
   *
   * @return integer|null
   *   The media entity ID or NULL if none was found or created.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createMediaImage($media_type, $image_content) {

    $image = new Image($media_type, $image_content, $this->message, $this->config, $this->logger);
    if ($media_entity = $image->getMediaEntity()) {
      return $media_entity->id();
    } else {
      $this->logger->error('Image media entity could not be created for the component: <pre><code>@component</code></pre>', ['@component' => print_r($image_content, TRUE)]);
      return NULL;
    }
  }

}
