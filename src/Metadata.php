<?php

namespace Drupal\ww_publish;

use Drupal\Core\Config\Config;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;

class Metadata {

  /**
   * Metadata extracted from the metadata.json file.
   */
  private $metadata;

  /**
   * Error messages.
   */
  private $errors;

  /**
   * Configuration of the ww_publish module.
   *
   * @var \Drupal\Core\Config\Config
   */
  private $config;

  /**
   * Logger interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * WoodWing author.
   *
   * Current user in WoodWing, who publish the article.
   *
   * @var \Drupal\user\Entity\User
   */
  private $wwAuthor;

  /**
   * Constructor.
   *
   * @param Object $metadata
   *   Metadata extracted from the metadata.json file.
   * @param \Drupal\Core\Config\Config $config
   *   The 'ww_publish.settings' config.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct($metadata, Config $config, LoggerInterface $logger) {
    $this->metadata = $metadata;
    $this->config = $config;
    $this->logger = $logger;
    $this->errors = array();
    $this->wwAuthor = $this->identifyAuthor();
  }

  /**
   * Validate the metadata.
   *
   * @return bool
   */
  public function validate() {
    if ($this->config->get('debug_mode'))
      $this->logger->debug('Validate metadata.');

    if (!$this->metadata) {
      $this->errors[] = "The metadata are not set.";
      return FALSE;
    }

    if (!$this->config->get('content_type_field') || strlen($this->config->get('content_type_field')) < 5) {
      $this->errors[] = "The content type field is not set.";
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get metadata.
   *
   * @return Object
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * Returns a certain metadata for the given identifier.
   * @param string $identifier
   *   Either the parent identifier like ContentMetaData or a combined key
   *   separated by |, like BasicMetaData|ID.
   * @param string|null $child_identifier
   *   The child identifier, must be empty if the parent contains the |
   *   separator.
   *
   * @param string|null
   *   Either the requested metadata or NULL if it is not set.
   */
  public function getMetadataByIdentifier($identifier, $child_identifier = NULL, $third_identifier = NULL) {
    if (!$child_identifier) {
      list($identifier, $child_identifier) = explode('|', $identifier, 2);
    }
    if (!$third_identifier && \strpos('|', $child_identifier) !== FALSE) {
      list($child_identifier, $third_identifier) = explode('|', $child_identifier, 2);
    }

    if ($identifier == 'ExtraMetaData') {
      foreach ($this->metadata->MetaData->ExtraMetaData as $extraMetaDataField) {
        if ($extraMetaDataField->Property == $child_identifier) {
          return $extraMetaDataField->Values[0];
        }
      }
      return NULL;
    }

    if ($third_identifier) {
      if (!empty($this->metadata->MetaData->$identifier->$child_identifier->$third_identifier)) {
        return $this->metadata->MetaData->$identifier->$child_identifier->$third_identifier;
      }
      return;
    }

    if (!empty($this->metadata->MetaData->$identifier->$child_identifier)) {
      return $this->metadata->MetaData->$identifier->$child_identifier;
    }

    return NULL;
  }

  /**
   * Get WoodWing author.
   *
   * @return \Drupal\user\Entity\User
   */
  public function getAuthor() {
    return $this->wwAuthor;
  }

  /**
   * Get File name.
   *
   * @param integer $fileId
   *   ID of the related file.
   *
   * @param string $filePath
   *   Path to the file.
   *
   * @return string
   */
  public function getFileName($fileId, $filePath) {
    foreach ($this->metadata->Relations as $relation) {
      if ($relation->Child === $fileId) {
        return $relation->ChildInfo->Name . '.' .
          substr($relation->ChildInfo->Format, strpos($relation->ChildInfo->Format, "/") + 1);
      }
    }
    $this->logger->error('No file with the ID @file_id could be found in the metadata.', ['@file_id' => $fileId]);

    // If the filename could not be generated, then the original filename will be used.
    return basename($filePath);
  }

  /**
   * Identify the WoodWing Author in Drupal users.
   *
   * @return \Drupal\user\UserInterface|null
   *   The identified author or NULL if no author user entity could be
   *   identified.
   */
  private function identifyAuthor() {
    $wwAuthor = NULL;
    if ($wwUserId = $this->config->get('ww_user')) {
      $wwAuthor = User::load($wwUserId);
    }

    if ($wwUserField = $this->config->get('ww_user_field')) {
      $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
      $contentStationIdentifier = $fields[$wwUserField]->getThirdPartySetting('ww_publish', 'content_station_identifier');

      if ($contentStationIdentifier && $wwAuthorToFind = $this->getMetadataByIdentifier($contentStationIdentifier)) {

        // Find the related user.
        $users = \Drupal::entityTypeManager()
          ->getStorage('user')
          ->loadByProperties([$wwUserField => $wwAuthorToFind]);

        // Test if the user exists.
        if (is_array($users) && count($users) === 1) {
          $wwAuthor = reset($users);
        }
      }
    }

    retUrn $wwAuthor;
  }

}
